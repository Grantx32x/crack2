#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    // Compare the two hashes
    // Free any malloc'd memory

    if(hash == md5(guess, HASH_LEN))
    {
        return 1;
    }
    free(hash);
    return 0;
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    int size = 100;
    char **pwds = (char **)malloc(size*sizeof(char*));
    char *str = (char*)malloc(40*sizeof(char));
    int i = 0;
    
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        printf("Can't open %s for reading\n", filename);
        exit(1);
    }
    
    while(fscanf(f, "%s\n", str) != EOF)
    {
        if(i == size)
        {
            size = size * 20;
            char** newarr = (char**)realloc(pwds, size*sizeof(char*));
            
            if(newarr != NULL) pwds = newarr;
            else
            {
                printf("Realloc failed.\n");
                exit(1);
            }
        }
        char *newstr = (char*)malloc(strlen(str)*sizeof(char));
        strcpy(newstr, str);
        pwds[i] = newstr;
        i++;
    }
    return pwds;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    
    for(int i = 0; i < 20; i++)
    {
        int j = 0;
        while(tryguess(hashes[i],dict[j])==0)
        {
            j++;
        }
        printf("%s %s\n",dict[j], hashes[i]);
    }
    
    
    
}
